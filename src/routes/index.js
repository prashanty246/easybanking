// const router = require("./users");
const users = require('./users')
const account = require('./account')
const transaction = require('./transaction')

const routes = [users, account, transaction]

module.exports = routes
